from __future__ import absolute_import, unicode_literals
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "adgroupnbp.settings")

import django
django.setup()

import json
from urllib2 import urlopen
from decimal import Decimal
from celery import Celery
from django.conf import settings  # noqa

from gold.models import Gold

app = Celery('tasks', broker='pyamqp://guest@localhost//')

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()

app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

def other_prices(pln, exchange):
    price = pln / exchange
    return price

@app.task
def exchange_download():
    table = 'a'
    format_ = 'json'
    topCount = 30

    _url_exchangerates = 'http://api.nbp.pl/api/exchangerates/rates/{table}/{code}/last/{topCount}/?format={format}'
    _url_gold = 'http://api.nbp.pl/api/cenyzlota/last/{topCount}/?format={format}'

    url_gold = _url_gold.format(topCount=topCount, format=format_)
    url_exchangerates_usd = _url_exchangerates.format(table=table, code='usd', topCount=topCount, format=format_)
    url_exchangerates_eur = _url_exchangerates.format(table=table, code='eur', topCount=topCount, format=format_)

    gold_json = json.load(urlopen(url_gold))
    usd_json = json.load(urlopen(url_exchangerates_usd))
    euro_json = json.load(urlopen(url_exchangerates_eur))

    for i in range(30):
        date = gold_json[i]['data']
        pln = Decimal(gold_json[i]['cena']).quantize(Decimal('0.01'))

        exchange_usd = Decimal(usd_json['rates'][i]['mid'])
        exchange_euro = Decimal(euro_json['rates'][i]['mid'])

        usd = other_prices(pln, exchange_usd).quantize(Decimal('0.01'))
        euro = other_prices(pln, exchange_euro).quantize(Decimal('0.01'))
        p, created = Gold.objects.get_or_create(date=date, pln=pln, usd=usd, euro=euro)
