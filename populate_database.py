from urllib2 import urlopen
import json
from decimal import Decimal
from gold.models import Gold

def other_prices(pln, exchange):
    price = pln / exchange
    return price


table = 'a'
format_ = 'json'
topCount = 30

_url_exchangerates = 'http://api.nbp.pl/api/exchangerates/rates/{table}/{code}/last/{topCount}/?format={format}'
_url_gold = 'http://api.nbp.pl/api/cenyzlota/last/{topCount}/?format={format}'

url_gold = _url_gold.format(topCount=topCount, format=format_)
url_exchangerates_usd = _url_exchangerates.format(table=table, code='usd', topCount=topCount, format=format_)
url_exchangerates_eur = _url_exchangerates .format(table=table, code='eur', topCount=topCount, format=format_)


gold_json = json.load(urlopen(url_gold))
usd_json = json.load(urlopen(url_exchangerates_usd))
euro_json = json.load(urlopen(url_exchangerates_eur))

for i in range(30):
    date = gold_json[i]['data']
    pln = Decimal(gold_json[i]['cena']).quantize(Decimal('0.01'))

    exchange_usd = Decimal(usd_json['rates'][i]['mid'])
    exchange_euro = Decimal(euro_json['rates'][i]['mid'])

    usd = other_prices(pln, exchange_usd).quantize(Decimal('0.01'))
    euro = other_prices(pln, exchange_euro).quantize(Decimal('0.01'))
    p, created = Gold.objects.get_or_create(date=date,pln=pln,usd=usd,euro=euro)
