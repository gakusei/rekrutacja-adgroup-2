# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Gold(models.Model):

    date = models.DateTimeField(unique=True)
    pln = models.DecimalField(max_digits=5, decimal_places=2)
    usd = models.DecimalField(max_digits=5, decimal_places=2)
    euro = models.DecimalField(max_digits=5, decimal_places=2)
