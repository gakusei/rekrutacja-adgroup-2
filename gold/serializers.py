from .models import Gold
from rest_framework import serializers


class GoldSerializer(serializers.ModelSerializer):
    class Meta:
        model = Gold
        fields = ("date", "pln", "usd", "euro")
