# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
from django.db.models import Avg
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework.views import APIView

from .models import Gold
from .serializers import GoldSerializer


class GoldViewSet(viewsets.ModelViewSet):
    queryset = Gold.objects.all().order_by('-date')
    serializer_class = GoldSerializer

class GoldAverageView(APIView):
    def get(self, request):
        now = datetime.now()
        days = 0
        average_dict = {}

        for i in range(4):
            week = "Tydzień " + str(i + 1)
            weekly_avg = Gold.objects.filter(date__lte=now - timedelta(days=days),
                                              date__gt=now - timedelta(days=(days+7))).values(
                'pln').aggregate(Avg('pln'))
            weekly_avg["week"] = str(i + 1)
            average_dict[week] = weekly_avg
            days += 7
        return Response(average_dict)
