from django.views.generic import TemplateView
from rest_framework.routers import SimpleRouter
from gold.views import GoldViewSet, GoldAverageView
from rest_framework_swagger.views import get_swagger_view
from django.conf.urls import url, include

router = SimpleRouter()
router.register(r'exchange', GoldViewSet)
schema_view = get_swagger_view(title='swagger')

urlpatterns = [
    url(r'^$', schema_view),
    url(r'^', include(router.urls)),
    url(r'index/', TemplateView.as_view(template_name = 'index.html')),
    url(r'average/', GoldAverageView.as_view()),
]
