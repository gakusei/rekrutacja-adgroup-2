# AdGroup NBP task

```
Stwórz skrypt, który pobierze z NBP Web API (http://api.nbp.pl) kursy cen złota z 30 ostatnich dni i wrzuci te dane do bazy danych. Następnie przy wykorzystaniu zapytania SQL wyświetl średnią cenę złota w każdym tygodniu bazując na przechowywanych danych.
Przy wykorzystaniu tego samego API przedstaw ceny w PLN / USD / EUR każdorazowo dla każdego dnia.
```

## Getting Started

install requirements.txt

	`pip install requirements.txt`

migrate database

	`manage.py makemigrations`
	`manage.py migrate`

## Celery
install RabbitMQ for example:

	`sudo apt-get install rabbitmq-server`

run celery from project directory:

	`celery -A tasks worker --loglevel=info`

## Test Populate database:
To fast populate database run from project directory:

	`./manage.py shell < populate_database.py`

## PAGES 
http://localhost:8000/index/ - index page with jquery and two endpoints rendered
http://localhost:8000/exchange/ - gold exchange endpoint
http://localhost:8000/average/ - gold average price from 4 weeks endpoint






