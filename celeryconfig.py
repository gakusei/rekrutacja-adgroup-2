from celery import app
from celery.schedules import crontab

broker_url = 'pyamqp://'
result_backend = 'rpc://'
imports = ('adgroupnbp.tasks',)

task_serializer = 'json'
result_serializer = 'json'
accept_content = ['json']
timezone = 'Europe/Oslo'
enable_utc = True

CELERYBEAT_SCHEDULE = {
    'every-monday-morning': {
       'task': 'tasks.exchange_download',
       'schedule': crontab(minute=0, hour=0),  # Will run everyday midnight
   },
}
